var config = {
    type: Phaser.AUTO,
    width: 1200,
    height: 1080,
    backgroundColor: 'white',
    scene: {
        preload: preload,
        create: create,
        update: update,
    },
};

var game = new Phaser.Game(config);

function preload() {
    this.load.image('board', 'assets/board.png');
    this.load.image('whiteStone', 'assets/whiteStone.png');
    this.load.image('blackStone', 'assets/blackStone.png');
}

function create() {
    this.add.image(540, 540, 'board').setScale(0.8, 0.8); //.scale.setTo(0.5, 0.5);
 //   var white = this.textures.get('witheStone').getFrameNames();
 //   var black = this.textures.get('blackStone').getFrameNames();
    
    var x = 50;
    var y = 50;
    const dropZone4 = {
        fieldState: false,
        "xPos": 720,
        "yPos": 250,
        "width": 50,
        "height": 50
    }
    console.log(dropZone4["xPos"])    

    for (var i = 0; i < 18; i++) {
        var blackStone = this.add.image(x,y, 'blackStone', Phaser.Math.RND.pick(frames)).setInteractive().setScale(0.4, 0.4);
        var whiteStone = this.add.image(x,y, 'whiteStone', Phaser.Math.RND.pick(frames)).setInteractive().setScale(0.4, 0.4);
        this.input.setDraggable(whiteStone);
        this.input.setDraggable(blackStone);

        y += 6;
    }
    
    var zone = this.add.zone(148, 148, 50, 50).setRectangleDropZone(50, 50);
    var zone2 = this.add.zone(540, 148, 50, 50).setRectangleDropZone(50, 50);
    var zone3 = this.add.zone(932, 148, 50, 50).setRectangleDropZone(50, 50);
    var zone4 = this.add.zone(dropZone4["xPos"], dropZone4["yPos"], dropZone4["width"], dropZone4["height"]).setRectangleDropZone(50, 50);

    //  Just a visual display of the drop zone
    var graphics = this.add.graphics();
    graphics.lineStyle(2, 0xffff00);
    graphics.strokeRect(zone.x - zone.input.hitArea.width / 2, zone.y - zone.input.hitArea.height / 2, zone.input.hitArea.width, zone.input.hitArea.height);
    graphics.strokeRect(zone2.x - zone2.input.hitArea.width / 2, zone2.y - zone2.input.hitArea.height / 2, zone2.input.hitArea.width, zone2.input.hitArea.height);
    graphics.strokeRect(zone3.x - zone3.input.hitArea.width / 2, zone3.y - zone3.input.hitArea.height / 2, zone3.input.hitArea.width, zone3.input.hitArea.height);
    graphics.strokeRect(zone4.x - zone4.input.hitArea.width / 2, zone4.y - zone4.input.hitArea.height / 2, zone4.input.hitArea.width, zone4.input.hitArea.height);

    this.input.on('dragstart', function (pointer, gameObject) {

        this.children.bringToTop(gameObject);

    }, this);

    this.input.on('drag', function (pointer, gameObject, dragX, dragY) {

        gameObject.x = dragX;
        gameObject.y = dragY;

    });

    this.input.on('dragenter', function (pointer, gameObject, dropzone) {

        //console.log('dragenter', dropzone);
        graphics.clear();
        graphics.lineStyle(2, 0x00ffff);
        graphics.strokeRect(zone.x - zone.input.hitArea.width / 2, zone.y - zone.input.hitArea.height / 2, zone.input.hitArea.width, zone.input.hitArea.height);
        graphics.strokeRect(zone2.x - zone2.input.hitArea.width / 2, zone2.y - zone2.input.hitArea.height / 2, zone2.input.hitArea.width, zone2.input.hitArea.height);
        graphics.strokeRect(zone3.x - zone3.input.hitArea.width / 2, zone3.y - zone3.input.hitArea.height / 2, zone3.input.hitArea.width, zone3.input.hitArea.height);
        graphics.strokeRect(zone4.x - zone4.input.hitArea.width / 2, zone4.y - zone4.input.hitArea.height / 2, zone4.input.hitArea.width, zone4.input.hitArea.height);

    });

    this.input.on('dragleave', function (pointer, gameObject, dropZone) {

        //console.log('dragleave');
        graphics.clear();
        graphics.lineStyle(2, 0xffff00);
        graphics.strokeRect(zone.x - zone.input.hitArea.width / 2, zone.y - zone.input.hitArea.height / 2, zone.input.hitArea.width, zone.input.hitArea.height);
        graphics.strokeRect(zone2.x - zone2.input.hitArea.width / 2, zone2.y - zone2.input.hitArea.height / 2, zone2.input.hitArea.width, zone2.input.hitArea.height);
        graphics.strokeRect(zone3.x - zone3.input.hitArea.width / 2, zone3.y - zone3.input.hitArea.height / 2, zone3.input.hitArea.width, zone3.input.hitArea.height);
        graphics.strokeRect(zone4.x - zone4.input.hitArea.width / 2, zone4.y - zone4.input.hitArea.height / 2, zone4.input.hitArea.width, zone4.input.hitArea.height);
               
    });

    this.input.on('drop', function (pointer, gameObject, dropZone) {
        
            gameObject.x = dropZone.x;
            gameObject.y = dropZone.y;
            
            console.log('drop -> ', dropZone);

        });      
    

    this.input.on('dragend', function (pointer, gameObject, dropped) {
        //console.log('gameObject -> ', gameObject)
        //console.log(gameObject.active)



        if (!dropped)
        {
            gameObject.x = gameObject.input.dragStartX;
            gameObject.y = gameObject.input.dragStartY;
            console.log('!dropped -> ', gameObject);
        }
    

        graphics.clear();
        graphics.lineStyle(2, 0xffff00);
        graphics.strokeRect(zone.x - zone.input.hitArea.width / 2, zone.y - zone.input.hitArea.height / 2, zone.input.hitArea.width, zone.input.hitArea.height);
        graphics.strokeRect(zone2.x - zone2.input.hitArea.width / 2, zone2.y - zone2.input.hitArea.height / 2, zone2.input.hitArea.width, zone2.input.hitArea.height);
        graphics.strokeRect(zone3.x - zone3.input.hitArea.width / 2, zone3.y - zone3.input.hitArea.height / 2, zone3.input.hitArea.width, zone3.input.hitArea.height);
        graphics.strokeRect(zone4.x - zone4.input.hitArea.width / 2, zone4.y - zone4.input.hitArea.height / 2, zone4.input.hitArea.width, zone4.input.hitArea.height);
        
    });    


}

function update() {

}